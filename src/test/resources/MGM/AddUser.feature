#Feature Definition MGM Mobile App

Feature: Adding or Inviting User 
Description: This section will add 1 user at a time

Background: 
	Given I currently logged in with <mobile number>
	When button is hit
	And I entered the verification <code>
	
Scenario Outline: Inviting friends to promote Imfree the app and certain rewards is awaits for the user
	When I tapped the Imfree brand disc
	And I tapped the "+" button to add friends
	And I entered the <friends number>
  Then inivted friend is successfully added
  And <rewards> is displayed
  
Examples:
		| friend numner	| rewards | mobile number  | code |
		|								|					|								 |      |


#Manual entering of mobile number

@manualinput
Scenario Outline: Title of your scenario outline
Given I want to write a step with <name>
When I check for the <value> in step
Then I verify the <status> in step


   