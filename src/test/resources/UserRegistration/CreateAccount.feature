#Feature Definition for Monile App

Feature: Create an Account
Description: Mobile user shoule be able to create Imfree account


Scenario: Accepting Terms and Privacy
Given I accepted the terms of use and privacy policy
When tapping the button
Then is successfully accepted the terms and privacy


Scenario Outline: Birthdate selection
Given I scrolled up and down to select birth <month>
And I scrolled up and down to select birth <date>
And I scrolled up and down to select birth <year>
When button is hit
Then birthdate selection is successfuly created

Examples:
    | month  | date |	year |
    |        |      |			 |
    |        |      | 		 |


@locationISdetected
Scenario Outline: Entering user information with location IS detected automatically 
Given I entered the <firstname>
And I entered the <lastname>
And selected the <gender>
When button is hit
Then information is saved successfully
And is redirected to mobile number page

Examples:
    | firstname  | lastname |	gender |
    |      		   |    		  |				 |
    |     		   |    		  | 			 |

@locationNOTdetected
Scenario Outline: Entering user information with location is NOT detected automatically 
Given I entered the <firstname>
And I entered the <lastname>
And selected the <gender>
And selected the <location>
When button is hit
Then information is saved successfully
And is redirected to mobile number page

Examples:
    | firstname  | lastname |	gender | location |
    |      		   |    		  |				 |					|
    |     		   |    		  | 			 |					|


# Mobile Number 

Scenario Outline: Entering valid mobile number 
Given I entered the valid <mobile number>
When button is hit
Then mobile number is successfully saved
And redirected to validation code page

Examples:
    | mobile number  |
    |								 |


# Validation Code 

Scenario Outline: Entering valid validation code 
Given I entered the valid validation <coder>
When button is hit
Then validation code is successfully accepted and saved
And redirected to welcome page

Examples:
    | code  |
    |				|



#Negative Testing Birthday Selection

@invalidBOD
Scenario Outline: Birthdate selection
Given I scrolled up and down to select birth <month>
And I scrolled up and down to select birth <date>
And I scrolled up and down to select birth <year>
When button is hit
Then an error message is displayed

Examples:
    | month  | date |	year |
    |        |      |			 |
    |        |      | 		 |
    
    
@invalidMobNum
Scenario Outline: Entering valid mobile number 
Given I entered invalid <mobile number>
Then error message is displayed

Examples:
    | mobile number  |
    |								 |
    