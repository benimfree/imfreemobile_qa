# Feature Definition Mobile App

Feature: Login
Description: User wants to login

Background: 

Scenario Outline: Logging in on the app
Given I logged in by entering the  <mobile number>
When button is hit
And I entered the verification <code>
Then I successfully logged in to the app

Examples:
    | mobile number  | code |
    |          			 |      |
    |          			 |      |